import logging

logging.getLogger().setLevel("DEBUG")

logger = logging.getLogger("knx")

import knxip.ip
import time

tunnel = knxip.ip.KNXIPTunnel(host_server_control_port=55555, host_server_data_port=55556, host_server_ip="192.168.0.7", ip="192.168.0.12")

def set_level(group, value, sleep = 2, packet_size = 0):
    logger.debug("Setting group {0} to {1}".format(group, value))
    tunnel.group_write(group, [value], packet_size)
    if sleep:
        time.sleep(sleep)
try:
    tunnel.connect()
    set_level(2, 24, 2, 1)
    set_level(2, 72, 2)
    set_level(1, 0, 0.25)
    set_level(2, 192, 2)
    set_level(2, 128, 2)
    set_level(1, 0, 0)
finally:
    tunnel.disconnect()
