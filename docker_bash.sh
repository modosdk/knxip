#!/usr/bin/env bash

set -x

docker build . -t knxip:dev
docker run -it --rm -v $(pwd):/knxip -p 55555:55555/udp -p 55556:55556/udp knxip:dev bash

