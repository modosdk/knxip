# pknx
Native KNX/IPNET library for Python

This library uses an IP socket connection to a KNX IP interface (like the Weinzierl 730). It does not need knxd or any
other external program to control devices on the KNX bus. 

The library support reads and writes to group addresses. It actively listens to the KNX bus and caches the state of
every group address. This greatly reduces the traffic on the KNX bus as read operations are usually fetched from the
cache.

# MODOS fork

This library is a fork of `https://github.com/open-homeautomation/pknx`, made because of a need to run the KNX client in a docker container. This presents some challenges, because the tunnelling protocol for KNX IP is UDP based and the protocol itself contains meta data about IP addressing and ports. This library relies on system calls (socket.gethost* calls)  to discover it's own IP address. When running a Dockerized application, the idiomatic/default network configuration means the application gets it's own virtual network interface and cannot bind the to the host network interface. As a result, these Python calls will resolve the IP address and port inside the container, rather than the IP and port of the host.

Since I have found no way of discovering host network configuration from inside the container, this information must now be supplied explicitly through your own means.

Notes:

- The broadcast/gateway scanner feature does not work inside docker. The IP address of the KNX IP gateway must be specified explicitly when initializing `KNXIPTunnel` objects.
- KNX Tunnelling requires you to listen on two UDP ports of your choice. One for "control" and one for "data".